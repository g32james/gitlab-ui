/* eslint-disable import/no-default-export */
export default {
  'ClearIconButton.title': 'Clear',
  'GlBreadcrumb.showMoreLabel': 'Show more breadcrumbs',
  'GlCollapsibleListbox.srOnlyResultsLabel': null,
  'GlKeysetPagination.navigationLabel': 'Pagination',
  'GlKeysetPagination.nextText': 'Next',
  'GlKeysetPagination.prevText': 'Previous',
  'GlPagination.labelPage': 'Go to page %{page}',
  'GlSearchBoxByType.clearButtonTitle': 'Clear',
  'GlSearchBoxByType.input.placeholder': 'Search',
  'GlSorting.sortAscending': 'Sort direction: ascending',
  'GlSorting.sortDescending': 'Sort direction: descending',
};
